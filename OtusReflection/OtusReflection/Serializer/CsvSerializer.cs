using System;
using System.Linq;
using System.Reflection;
using System.Text;
using OtusReflection.Interfaces;

namespace OtusReflection.Serializer
{
    public class CsvSerializer : ISerializer
    {
        private readonly char _separator;
        
        public CsvSerializer(char separator = ';')
        {
            if (separator == ':') throw new ArgumentException("Invalid separator. Separator can not be ':'.");
            _separator = separator;
        }
        
        public T Deserialize<T>(string stringObj)
        {
            if (string.IsNullOrEmpty(stringObj)) throw new ArgumentNullException(nameof(stringObj));

            var deserializeObject = Activator.CreateInstance<T>();

            var members = deserializeObject.GetType().GetMembers();
            
            foreach (var serializeMember in stringObj.Split(_separator, StringSplitOptions.RemoveEmptyEntries))
            {
                var name = serializeMember.Split(':')[0];
                var value = serializeMember.Split(':')[1];

                var objectProperty = members.SingleOrDefault(member =>
                    member.Name.Equals(name) && member.MemberType == MemberTypes.Property) as PropertyInfo;
                
                var objectField = members.SingleOrDefault(member =>
                    member.Name.Equals(name) && member.MemberType == MemberTypes.Field) as FieldInfo;

                if (objectProperty != null)
                {
                    objectProperty.SetValue(deserializeObject, Convert.ChangeType(value, objectProperty.PropertyType));
                }
                else if (objectField != null)
                {
                    objectField.SetValue(deserializeObject, Convert.ChangeType(value, objectField.FieldType));
                }
                else
                {
                    throw new Exception($"Not found member with name {name}.");
                }
                
            }

            return deserializeObject;
        }
        

        public string Serialize(object obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            var serializeProperties =
                from property in obj.GetType().GetProperties()
                select new {Name = property.Name, Value = property.GetValue(obj)};
            var serializeFields = 
                from field in obj.GetType().GetFields()
                select new {Name = field.Name, Value = field.GetValue(obj)};

            var serializeObject = new StringBuilder();

            foreach (var property in serializeProperties)
            {
                serializeObject.Append($"{property.Name}:{property.Value}{_separator}");
            }

            foreach (var field in serializeFields)
            {
                serializeObject.Append($"{field.Name}:{field.Value}{_separator}");
            }

            return serializeObject.ToString();
        }
    }
}