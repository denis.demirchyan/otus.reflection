﻿using System;
using System.Diagnostics;
using OtusReflection.Serializer;
using Newtonsoft.Json;

namespace OtusReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            var iterations = 100000;
            var testClass = TestClass.Get();

            var mySerializer = new CsvSerializer();
            var mySerialized = mySerializer.Serialize(testClass);

            var newtonsoftSerialized = JsonConvert.SerializeObject(testClass);
            
            var sw = Stopwatch.StartNew();
            for (var i = 0; i < iterations; ++i)
            {
                mySerializer.Serialize(testClass);
            }
            sw.Stop();
            Console.WriteLine($"mySerializer. Serialize time: {(double)(sw.ElapsedMilliseconds) / iterations} ms.{Environment.NewLine}");

            sw.Start();
            for (var i = 0; i < iterations; ++i)
            {
                mySerializer.Deserialize<TestClass>(mySerialized);
            }
            sw.Stop();
            Console.WriteLine($"mySerializer. Deserialize time: {(double)(sw.ElapsedMilliseconds) / iterations} ms.{Environment.NewLine}");
            
            sw.Start();
            for (var i = 0; i < iterations; ++i)
            {
                JsonConvert.SerializeObject(testClass);
            }
            sw.Stop();
            Console.WriteLine($"Newtonsoft. Serialize time: {(double)(sw.ElapsedMilliseconds) / iterations} ms.{Environment.NewLine}");

            sw.Start();
            for (var i = 0; i < iterations; ++i)
            {
                JsonConvert.DeserializeObject<TestClass>(newtonsoftSerialized);
            }
            sw.Stop();
            Console.WriteLine($"Newtonsoft. Deserialize time: {(double)(sw.ElapsedMilliseconds) / iterations} ms.{Environment.NewLine}");
        }
    }
}