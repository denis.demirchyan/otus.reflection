namespace OtusReflection.Interfaces
{
    public interface ISerializer
    {
        T Deserialize<T>(string stringObj);

        string Serialize(object obj);
    }
}