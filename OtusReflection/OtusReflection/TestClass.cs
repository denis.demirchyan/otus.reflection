#nullable enable
using System;

namespace OtusReflection
{
    public class TestClass
    {
        public string StringValue;
        
        public int IntValue { get; set; }

        public TestClass()
        {
            
        }

        public TestClass(string stringValue, int intValue)
        {
            StringValue = stringValue;
            IntValue = intValue;
        }

        public static TestClass Get()
        {
            return new TestClass("test string", 101);
        }

        public override bool Equals(object? obj)
        {
            return obj != null && obj.GetType() == typeof(TestClass) && this.Equals(obj as TestClass);
        }

        protected bool Equals(TestClass other)
        {
            return StringValue == other.StringValue && IntValue == other.IntValue;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StringValue, IntValue);
        }
    }
}